import threading
import getopt
import time
import sys

ABC = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'


def escribir(s, h, f, r, abc):
    with s:
        for i in range(r):
            a = abc[h]
            f.write(a)
            f.flush()
            print(h, a, i)
            time.sleep(1)


try:
    (opt, arg) = getopt.getopt(sys.argv[1:], 'f:n:r:', [])
except getopt.GetoptError as err:
    print(err)
    sys.exit(1)


try:
    for (op, ar) in opt:
        if (op == '-f'):
            arch = ar
        elif (op == '-r'):
            cant = int(ar)
        elif (op == '-n'):
            hilos = int(ar)
        else:
            sys.exit(2)
except ValueError:
    print("El argumento de -n y -r debe ser un numero")
    sys.exit()

sem = threading.BoundedSemaphore(1)

f = open(arch, "w")

for h in range(hilos):
    threading.Thread(target=escribir, args=(sem, h, f, cant, ABC,)).start()
