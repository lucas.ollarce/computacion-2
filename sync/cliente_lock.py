import sys
import socket
import getopt
import time


try:
    (opt, arg) = getopt.getopt(sys.argv[1:], 'h:p:c:r:')
except getopt.GetoptError as err:
    print(err)
    sys.exit(1)

try:
    for (op, ar) in opt:
        if (op == '-h'):
            print(ar)
            host = ar
        elif (op == '-p'):
            print(ar)
            port = int(ar)
        elif (op == '-c'):
            print(ar)
            letra = ar
        elif (op == '-r'):
            print(ar)
            cant = ar
        else:
            sys.exit(2)
except ValueError:
    print("El argumento de -p y -r debe ser un numero")
    sys.exit()

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.connect((host, port))
except ConnectionRefusedError:
    print("Conexion rechazada")
    sys.exit()

msg = f"letra:{letra}"
msg1 = f"n:{cant}"
s.send(msg.encode('utf8'))
time.sleep(0.1)
s.send(msg1.encode('utf8'))
s.close()
