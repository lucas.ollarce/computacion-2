import threading as th
import socket
import getopt
import time
import sys


def server(c, s, f):
    print("Iniciando hilo...")
    sock, addr = c 
    l = ""
    r = 0
    while True:
        msg = clientsocket.recv(1024).decode('utf-8')
        if msg[0:5] == "letra":
            l = msg[6:]
        elif msg[0:1] == "n":
            r = int(msg[2:])
        else:
            break
        
    print(f"Dirección: {str(addr)}")
    print(f"Recibido: {l} {r}")
    with s:
        for i in range(r):
            f.write(l)
            f.flush()
            print(str(addr), l, i)
            time.sleep(1)




try:
    (opt, arg) = getopt.getopt(sys.argv[1:], 'p:f:')
except getopt.GetoptError as err:
    print(err)
    sys.exit(1)


try:
    for (op, ar) in opt:
        if (op == '-p'):
            port = int(ar)
        elif (op == '-f'):
            arch = ar
        else:
            sys.exit(2)
except ValueError:
    print("El argumento de -p debe ser un numero")
    sys.exit()

sem = th.BoundedSemaphore(1)

f = open(arch, "w")

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

host = ""
try:
    serversocket.bind((host, port))
except NameError:
    print("Puerto no definido")
    print("Utilice -p para definir el puerto")
    sys.exit()
except OverflowError:
    print("El puerto debe ser entre 0-65535")
    sys.exit()
except PermissionError:
    print("Permiso denegado")
    sys.exit()

serversocket.listen(5)
print("Esperando conexiones...")

while True:
    cliente = serversocket.accept()
    clientsocket, addr = cliente
    print("Cliente %s conectado" % str(addr))

    th.Thread(target=server, args=(cliente, sem, f)).start()

clientesocket.close()
