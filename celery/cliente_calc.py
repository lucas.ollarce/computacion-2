import sys
import socket
import getopt


try:
    (opt, arg) = getopt.getopt(sys.argv[1:], 'h:p:o:n:m:')
except getopt.GetoptError as err:
    print(err)
    sys.exit(2)

try:
    for (op, ar) in opt:
        if op == '-h':
            host = ar
        elif op == '-p':
            port = int(ar)
        elif op == '-o':
            oper = ar
        elif op == '-n':
            n1 = ar
        elif op == '-m':
            n2 = ar
        else:
            sys.exit(2)
except ValueError:
    print("El argumento de -p debe ser un numero")
    sys.exit()

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error:
    print("No se pudo crear el socket")
    sys.exit()

try:
    s.connect((host, port))
except:
    sys.exit()

ok = '200'

s.send(oper.encode('utf-8'))
data = s.recv(512).decode('utf-8')
if data == ok:
    s.send(n1.encode('utf-8'))
    data = s.recv(512).decode()
    if data == ok:
        s.send(n2.encode('utf-8'))
        data = s.recv(512).decode()
        print(data)
        s.close()
        sys.exit()
