import sys
import time
import getopt
import socket
import importlib
import calc_celery as c


def calc(oper,n1,n2):
    n = int(n1)
    m = int(n2)
    try:
        if oper == 'suma':
            r = c.suma.delay(n,m)
        elif oper == 'resta':
            r = c.resta.delay(n,m)
        elif oper == 'mult':
            r = c.mult.delay(n,m)
        elif oper == 'div':
            r = c.div.delay(n,m)
        elif oper == 'pot':
            r = c.pot.delay(n,m)
        else:
            return "argumento invalido"
        return r.get()
    except Exception as err:
        return str(err)


try:
    (opt, args) = getopt.getopt(sys.argv[1:], 'h:p:')
except getopt.GetoptError as err:
    print(err)
    sys.exit(2)

for (op, ar) in opt:
    if op == '-h':
        host = ar
    elif op == '-p':
        port = int(ar)
    else:
        sys.exit()
        
serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    serversocket.bind((host, port))
except Exception as err:
    print(err)
    sys.exit()

serversocket.listen(5)
print(f"Esperando conexiones en {host}:{port}")

resp = "200"

while True:
    clientsocket, addr = serversocket.accept()
    oper = clientsocket.recv(512).decode()
    time.sleep(.1)
    clientsocket.send(resp.encode('utf-8'))
    n1 = clientsocket.recv(512).decode()
    time.sleep(.1)
    clientsocket.send(resp.encode('utf-8'))
    n2 = clientsocket.recv(512).decode()
    print(f"Dirección: {str(addr)}")
    print(f"Recibido:\n\tOperación: {oper}\n\tPrimer número: {n1}\n\tSegundo número: {n2}")
    time.sleep(.1)
    calcular = calc(oper, n1, n2)
    clientsocket.send(str(calcular).encode('utf-8'))
