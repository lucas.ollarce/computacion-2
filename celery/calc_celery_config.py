from celery import Celery

app = Celery('calc_celery', broker='redis://localhost:6379', backend='redis://localhost:6379',
        include=['calc_celery'])
