import socket
import subprocess as sp
import multiprocessing as mp


def mp_server(c):
    print("Iniciando proceso...")
    sock, addr = c
    while True:
        d = clientsocket.recv(1024)
        msg = d.decode("utf8")
        if len(d) == 0:
            print("Cliente %s desconectado" % str(addr))
            break
        print(f"Dirección: {str(addr)}")
        print(f"Recibido: {msg}")

        command = sp.Popen([msg], shell=True, stdout=sp.PIPE, stderr=sp.PIPE, text=True)
        stdout, stderr = command.communicate()

        if command.returncode == 0:
            resp = "OK \n"+stdout
        elif command.returncode != 0:
            resp = "ERROR \n"+stderr

        clientsocket.send(resp.encode('utf8'))


serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

host = ""
port = 9009

serversocket.bind((host, port))

serversocket.listen(5)

while True:
    cliente = serversocket.accept()
    clientsocket, addr = cliente
    print("Cliente %s conectado" % str(addr))

    child = mp.Process(target=mp_server, args=(cliente,))
    child.start()

clientsocket.close()
