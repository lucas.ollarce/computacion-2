import os
import sys
import time
import getopt
import socketserver
import subprocess as sp


class ForkingEchoRequestHandler(socketserver.BaseRequestHandler):
    def handle(self):
        print("Iniciando proceso...")
        #sock, addr = c
        while True:
            d = self.request.recv(1024)
            msg = d.decode("utf8")
            if len(d) == 0:
                print("Cliente desconectado")
                break
            print("------------------server")
            # print("Dirección: {}")
            print(f"Recibido: {msg}")

            command = sp.Popen([msg], shell=True, stdout=sp.PIPE, stderr=sp.PIPE, text=True)
            stdout, stderr = command.communicate()

            if command.returncode == 0:
                resp = "OK \n"+stdout
            elif command.returncode != 0:
                resp = "ERROR \n"+stderr

            self.request.send(resp.encode('utf8'))
            # return


class ThreadedEchoResquestHandler(socketserver.BaseRequestHandler):
    def handle(self):
        print("Iniciando hilo...")
        # sock, addr = c
        while True:
            d = self.request.recv(1024)
            msg = d.decode("utf8")
            if len(d) == 0:
                print("Cliente desconectado")
                break
            print("------------------server")
            # print(f"Dirección: {str(addr)}")
            print(f"Recibido: {msg}")

            command = sp.Popen([msg], shell=True, stdout=sp.PIPE, stderr=sp.PIPE, text=True)
            stdout, stderr = command.communicate()

            if command.returncode == 0:
                resp = "OK \n"+stdout
            elif command.returncode != 0:
                resp = "ERROR \n"+stderr

            self.request.send(resp.encode('utf8'))
            #return


class ForkingEchoServer(socketserver.ForkingMixIn, socketserver.TCPServer,):
    pass


class ThreadedEchoServer(socketserver.ThreadingMixIn, socketserver.TCPServer,):
    pass


if __name__ == '__main__':
    import socket
    import threading
    
    try:
        (opt, arg) = getopt.getopt(sys.argv[1:], 'm:')
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)

    try:
        for (op, ar) in opt:
            if (op == '-m'):
                option = ar
            else:
                sys.exit(2)
    except Exception:
        print("argumento invalido")
        sys.exit()

    address = ('localhost', 0)
    if option == 'p':
        server = ForkingEchoServer(address, ForkingEchoRequestHandler)
    elif option == 't':
        server = ThreadedEchoServer(address, ThreadedEchoResquestHandler)

    ip, port = server.server_address
    print(ip, port)

    #cliente = s.accept()
    #clientesocket, addr = cliente
    t = threading.Thread(target=server.serve_forever)
    t.setDaemon(True)
    t.start()
    if option == 'p':
        print("Server loop running in process:", os.getpid())
    elif option == 't':
        print("Server loop running in thread:", t.getName())

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((ip, port))
    
    time.sleep(0.1)
    while True:
        try:
            msg = input("> ")
            if msg == "":
                pass
            else:
                s.send(msg.encode('utf8'))
                data = s.recv(1024).decode('utf8')
                print("------------------cliente")
                print(data)
                if msg == 'exit':
                    print('exit')
                    server.shutdown()
                    s.close()
                    server.socket.close()
                    break
        except EOFError:
            server.shutdown()
            s.close()
            server.socket.close()
            break
