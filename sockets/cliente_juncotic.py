import sys
import socket
import getopt


def respuesta(data):
    if data == '200':
        print(f"{data}: OK")
    elif data == '400':
        print(f"{data}: Comando válido, pero fuera de secuencia.")
    elif data == '500':
        print(f"{data}: Comando inválido.")
    elif data == '404':
        print(f"{data}: Clave errónea.")
    elif data == '405':
        print(f"{data}: Cadena nula.")


try:
    (opt, arg) = getopt.getopt(sys.argv[1:], 'a:p:')
except getopt.GetoptError as err:
    print(err)
    sys.exit(2)

try:
    for (op, ar) in opt:
        if (op in ['-a']):
            host = ar
        elif (op in ['-p']):
            port = int(ar)
        else:
            sys.exit(2)
except ValueError:
    print("El argumento de -p debe ser un numero")
    sys.exit()

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error:
    print("No se pudo crear el socket")
    sys.exit()

try:
    s.connect((host, port))
except NameError:
    print("Dirección ip y/o puerto no definido")
    print("Utilice -a para definir la dirección ip")
    print("Utilice -p para definir el puerto")
    sys.exit()
except ConnectionRefusedError:
    print("Conexion rechazada")
    sys.exit()
except OverflowError:
    print("El puerto debe ser entre 0-65535")
    sys.exit()
except socket.error:
    print("Fallo temporal en la resolución de nombres")
    sys.exit()

ok = '200'

while True:
    nombre = input("Ingrese su nombre: ")
    msg = "hello|"+nombre
    s.send(msg.encode('ascii'))
    data = s.recv(512).decode("ascii")
    respuesta(data)
    if data == ok:
        while True:
            email = input("Ingrese su email: ")
            msg = "email|"+email
            s.send(msg.encode('ascii'))
            data = s.recv(512).decode("ascii")
            respuesta(data)
            if data == ok:
                while True:
                    key = input("Ingrese la key: ")
                    msg = "key|"+key
                    s.send(msg.encode('ascii'))
                    data = s.recv(512).decode("ascii")
                    respuesta(data)
                    if data == ok:
                        print("Finalizando conexión...")
                        msg = "exit"
                        s.send(msg.encode('ascii'))
                        data = s.recv(512).decode("ascii")
                        respuesta(data)
                        if data == ok:
                            s.close()
                            sys.exit(0)
