import sys
import getopt

try:
    (opt, arg) = getopt.getopt(sys.argv[1:], 'o:i:h')
except getopt.GetoptError as err:
    print(err)
    print("Utilice -h para obtener mas ayuda")
    sys.exit(2)

arch1 = 0
arch2 = 0

for (op, ar) in opt:
    if (op in ['-i']):
        if (arch1 == 0):
            arch1 = ar
    elif (op == '-o'):
        if (arch2 == 0):
            arch2 = ar
    elif (op == '-h'):
        print("Copiar\n")
        print(" -i       nombre del archivo a copiar")
        print(" -o       nombre del nuevo archivo donde pegar el contenido ")
        print(" -h       ayuda\n")
        sys.exit()

try:
    file = open(arch1, "r")
    list = file.readlines()
    file.close()

    copytext = open(arch2, "w")
    for i in list:
        copytext.write(i)
    copytext.close()
except FileNotFoundError:
    print("El archivo que ingreso no existe")
    sys.exit(2)
except NameError:
    print("copiar.py necesita mas argumentos para funcionar correctamente")
    print("Utilice -h para obtener mas ayuda")
    sys.exit(2)
