import os
import sys
import getopt


def hijo():
    pids = (os.getpid(), os.getppid())
    print("Soy el proceso %d, mi padre es %d" % pids)
    os._exit(0)


def padre(hijos):
    for i in range(hijos):
        ret = os.fork()
        if (ret == 0):
            hijo()
        else:
            os.wait()


hijos = 0

try:
    (opt, arg) = getopt.getopt(sys.argv[1:], 'n:')
except getopt.GetoptError as err:
    print(err)
    sys.exit(2)

try:
    for (op, ar) in opt:
        if (op in ['-n']):
            if (hijos == 0):
                hijos = int(ar)
        else:
            sys.exit(2)
except ValueError:
    print("El argumento debe ser un numero decimal")
    sys.exit()

padre(hijos)
