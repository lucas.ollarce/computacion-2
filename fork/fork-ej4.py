import os
import time


def main():
    ret = os.fork()

    if ret == 0:
        for i in range(5):
            print("Soy el hijo, PID", os.getpid())
        time.sleep(30)
        print("PID %d terminado" % os.getpid())
    else:
        pids = (os.getpid(), ret)
        for i in range(2):
            print("Soy el padre, PID %d, mi hijo es %d" % pids)
        os.wait()
        print("Mi proceso hijo, PID %d, terminó" % ret)
        time.sleep(5)


if __name__ == "__main__":
    main()
