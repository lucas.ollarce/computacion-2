import os
import sys
import getopt
import subprocess as sp
from datetime import datetime


def outputfile(outfile):
    if arch1 is True:
        outf = open(outfile, "a")
        outf.writelines(stdout)
    else:
        outf = open(outfile, "w")
        outf.writelines(stdout)


def log_file(logfile):
    correcto = (fecha+": Comando \""+command+"\" ejecutado correctamente\n")
    error = (fecha+" "+stderr)
    if arch2 is True:
        logf = open(logfile, "a")
        if (c.returncode == 0):
            logf.writelines(correcto)
        elif (c.returncode != 0):
            logf.writelines(error)
    else:
        logf = open(logfile, "w")
        if (c.returncode == 0):
            logf.writelines(correcto)
        elif (c.returncode != 0):
            logf.writelines(error)


def ayuda():
    print("\nEjecutor\n")
    print("uso: python3 ejecutor.py -c [comando] -f [archivo] -l [archivo]\n")
    print(" -c      comando a ejecutar entre comillas (\") o (\')")
    print(" -f      archivo en donde almacena la salida del comando")
    print(" -l      archivo en donde almacena el log del comando\n")
    print("ejemplo:")
    print("     python3 ejecutor.py -c \"ls -a /\" -f salida.txt -l log.txt")


try:
    (opt, arg) = getopt.getopt(sys.argv[1:], 'c:f:l:h')
except getopt.GetoptError as err:
    print(err)
    print("Utilice -h para obtener mas ayuda")
    sys.exit(2)

command = 0
outfile = 0
logfile = 0

for (op, ar) in opt:
    if (op in ['-c']):
        if (command == 0):
            command = ar
    elif (op == '-f'):
        if (outfile == 0):
            outfile = ar
    elif (op == '-l'):
        if (logfile == 0):
            logfile = ar
    elif (op == '-h'):
        print(ayuda())
        sys.exit()

if (command == 0 or outfile == 0 or logfile == 0):
    print("faltan opciones")
    print("Utilice -h para obtener mas ayuda")
    sys.exit()
else:
    c = sp.Popen([command], shell=True, stdout=sp.PIPE, stderr=sp.PIPE, text=True)
    stdout, stderr = c.communicate()

    arch1 = os.path.isfile(outfile)
    arch2 = os.path.isfile(logfile)

    fecha = datetime.today().strftime('%d/%m/%Y %H:%M:%S')
    outputfile(outfile)
    log_file(logfile)
